<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<main class="text-center">
    <div class="jumbotron">
        <img class="mb-4" src="https://freesvg.org/img/cinema-penguin.png" alt="" width="92" height="92"><h1 class="display-4">Кинотеатр "КИНОТЕАТР"</h1>
        <p class="lead">Добро пожаловать на сайт "КИНОТЕАТР". Вы можете ознакомиться с фильмами в прокате и сеансами.</p>
        <!--<a class="btn btn-primary btn-lg" href="#" role="button">Войти</a> -->
    </div>
</main>

<?= $this->endSection() ?>

<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div  class="container">
<h1>Все сеансы</h1>
<br><h5> На данной странице представлены фильмы в прокате. </h5></br>
<?php if (!empty($region) && is_array($region)) : ?>
    <div class="d-flex justify-content-between mb-2">
    <?= $pager->links('group1','my_page') ?>
    <?= form_open('FilmController/index', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
        </form>

        <?= form_open('FilmController/index',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Название фильма" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>

    </div>
    <table class="table">
    <thead class="text-white bg-primary">
        <tr>
            <th scope="col"> #</th>
            <th scope="col"> Фильм</th>
            <th scope="col"> Продолжительность</th>
            <th scope="col"> Подробнее</th>
           </tr>
    </thead>
    <?php
    $c=1; 
    foreach ($region as $item): ?>
    <tbody>
        <tr>
            <th scope='row'><?php echo $c ?></th>
            <td><?php echo $item['Movie_name'] ?></td>
            <td><?php echo $item['Length'] ?></td>
            <td><a href="<?= base_url()?>/index.php/FilmController/view/<?= esc($item['Movie_id']); ?>" class="btn btn-outline-primary"> Посмотреть</a></td>
       </tr> 
    <?php $c+=1; endforeach; ?> 
    </tbody>
    </table>
    <?php else : ?>
        <p>Невозможно найти сеансы.</p>
    <?php endif ?>
<br>
<br>
</div>

<?= $this->endSection() ?>

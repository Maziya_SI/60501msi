<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

<?= form_open_multipart('FilmController/update'); ?>
<input type="hidden" name="id" value="<?= $polling_station['id'] ?>">
<input type="hidden" name="Movie_id" value="<?= $polling_station['Movie_id'] ?>">

<div class="form-group">

<h5 class="card-title">Фильм: <?= esc($polling_station['Movie_name']); ?> </h5>

<div class="form-group">
<label for="StartDate">Дата: ( <?= $polling_station["StartDate"]; ?> )</label>
<input type="date_time" class="form-control <?= ($validation->hasError('StartDate')) ? 'is-invalid' : ''; ?>" name="StartDate"
               value="<?= $polling_station["StartDate"]; ?>">
<?= $validation->getError('StartDate') ?>
</div>

<div class="form-group">
<label for="Hall_id">Тип зала: ( <?= $polling_station["Hall_name"]; ?> )</label>
<select type="text" class="form-control <?= ($validation->hasError('Hall_id')) ? 'is-invalid' : ''; ?>" name="Hall_id"
               value="<?= $polling_station["Hall_id"]; ?>">
            <?php foreach ($H as $item): {
                echo "<option value=" . $item['Hall_id'] . ">" . $item['Hall_name'] . "</option>";
            } endforeach; ?>
</select> <?= $validation->getError('Hall_id') ?></div>



<label for="Price">Ценовая категория: ( <?= $polling_station["Price"]; ?> )</label>
<select type="text" class="form-control <?= ($validation->hasError('Price')) ? 'is-invalid' : ''; ?>" name="Price"
               value="<?= $polling_station["Price"]; ?>">
            <option value="250">250</option>
            <option value="400">400</option>
            <option value="650">650</option>
             ?>
</select> <?= $validation->getError('Price') ?></div>



<br>
<div class="form-group">
<button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
</div>
</form>
</div>
<?= $this->endSection() ?>



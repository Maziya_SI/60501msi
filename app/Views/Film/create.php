<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('FilmController/store'); ?>
    <div class="form-group">
        <label for="Movie_id">Фильм</label>
        <select type="text" class="form-control <?= ($validation->hasError('Movie_id')) ? 'is-invalid' : ''; ?>" name="Movie_id"
               value="<?= old('Movie_id'); ?>">
            <?php foreach ($F as $item): {
                echo "<option value=" . $item['Movie_id'] . ">" . $item['Movie_name'] . "</option>";
            } endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('Movie_id') ?>
        </div> <br>

        <div class="form-group">
        <label for="Hall_id">Зал</label>
        <select type="text" class="form-control <?= ($validation->hasError('Hall_id')) ? 'is-invalid' : ''; ?>" name="Hall_id"
               value="<?= old('Hall_id'); ?>">
            <?php foreach ($H as $item): {
                echo "<option value=" . $item['Hall_id'] . ">" . $item['Hall_name'] . "</option>";
            } endforeach; ?>
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('Hall_id') ?>
        </div> <br>

        <div class="form-group">
        <label for="Price">Ценовая категория</label>

<select type="text" class="form-control <?= ($validation->hasError('Price')) ? 'is-invalid' : ''; ?>" name="Price">
            <option value="250">250</option>
            <option value="400">400</option>
            <option value="650">650</option>
             ?>
</select> <?= $validation->getError('Price') ?></div>

        <div class="invalid-feedback">
            <?= $validation->getError('Price') ?>
        </div> <br>

        <div class="form-group">
        <label for="StartDate">Дата</label>
        <input type="date_time" class="form-control <?= ($validation->hasError('StartDate')) ? 'is-invalid' : ''; ?>" name="StartDate"
               value="2000-01-01 00:00:00">
        <div class="invalid-feedback">
            <?= $validation->getError('StartDate') ?>
        </div> <br>
        </div>

        <div class="form-group">
        <label for="StartDate">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div><br>

    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit" style="margin-left: auto; margin-right: auto;">Создать</button>
    </div>
    </form>


    </div>
<?= $this->endSection() ?>

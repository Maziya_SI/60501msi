<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container">
<h1>Сеансы на выбранный фильм</h1>
    <br><h5> На данной странице представлены все сеансы на выбрынный фильм в прокате. </h5></br>
    <?php if (!empty($polling_station) && is_array($polling_station)) : ?>
<?php foreach ($polling_station as $item): ?>
             <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center"> 
                    <?php if (is_null($item['picture_url'])) : ?>
                        <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/3439/3439702.svg" class="card-img">   
                    <?php else:?>
                        <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img">
                    <?php endif ?> 
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Фильм: <?= esc($item['Movie_name']); ?> </h5>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Дата:</div>
                                <div class="text-muted"><?= esc($item['StartDate']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Тип зала:</div>
                                <div class="text-muted"><?= esc($item['Hall_name']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Цена билета:</div>
                                <div class="text-muted"><?= esc($item['Price']); ?></div>
                            </div> <br><br>
                                    <?php if ( $ionAuth->loggedIn()): ?>  
                                            <br>
                                            <a class="btn btn-warning " href="<?= base_url()?>/index.php/FilmController/edit/<?php echo $item['id'] ?>">Изменить сеанс</a>
                                            <a class="btn btn-danger " href="<?= base_url()?>/index.php/FilmController/delete/<?php echo $item['id'] ?>">Удалить сеанс</a>
                                     <?php endif ?>    

                            <div class="d-flex justify-content-between"></div>
                        </div>
                    </div> 
                </div> 
            </div>
<?php endforeach; ?>
    <?php else : ?>
        <p>Фильмы не найдены.</p>
    <?php endif ?>
<br>
<br>
</div>

<?= $this->endSection() ?>

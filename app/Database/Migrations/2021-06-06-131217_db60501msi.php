<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class db60501msi extends Migration
{
	public function up()
	{
        // activity_type
        if (!$this->db->tableexists('hall'))
        {
            // Setup Keys
            $this->forge->addkey('Hall_id', TRUE);

            $this->forge->addfield(array(
                'Hall_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'Hall_name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE)
            ));
            // create table
            $this->forge->createtable('hall', TRUE);
        }

        if (!$this->db->tableexists('movie'))
        {
            // Setup Keys
            $this->forge->addkey('Movie_id', TRUE);

            $this->forge->addfield(array(
                'Movie_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'Movie_name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => true),
                'Length' => array('type' => 'time', 'null' => true)
            ));
            // create table
            $this->forge->createtable('movie', TRUE);
        }

        // activity_type
        

if (!$this->db->tableexists('seat'))
        {
            // Setup Keys
            $this->forge->addkey('Seat_id', TRUE);

            $this->forge->addfield(array(
                'Seat_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'Hall_id' => array('type' => 'int', 'unsigned' => TRUE, 'null' => FALSE),
                'Row_id' => array('type' => 'int', 'null' => false),
                'Number_id' => array('type' => 'int', 'null' => false),
                'Price_catg' => array('type' => 'int', 'null' => false)
            ));
            $this->forge->addForeignKey('Hall_id','hall','Hall_id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('seat', TRUE);
        }

if (!$this->db->tableexists('session'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'Hall_id' => array('type' => 'int', 'unsigned' => TRUE, 'null' => FALSE),
                'Movie_id' => array('type' => 'int', 'unsigned' => TRUE, 'null' => false),
                'StartDate' => array('type' => 'datetime', 'null' => TRUE),
                'Price' => array('type' => 'int', 'null' => false),
            ));
            $this->forge->addForeignKey('Hall_id','hall','Hall_id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('Movie_id','movie','Movie_id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('session', TRUE);
        }


if (!$this->db->tableexists('ticket'))
        {
            // Setup Keys
            $this->forge->addkey('Ticket_id', TRUE);

            $this->forge->addfield(array(
                'Ticket_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'Session_id' => array('type' => 'int', 'unsigned' => TRUE, 'null' => FALSE),
                'Seat_id' => array('type' => 'int', 'unsigned' => TRUE, 'null' => false),
                'FullName' => array('type' => 'varchar', 'constraint' => '255', 'null' => TRUE)
            ));
            $this->forge->addForeignKey('Session_id','session','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('Seat_id','seat','Seat_id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('ticket', TRUE);
        }
	}

	//--------------------------------------------------------------------

	public function down()
	{
if ($this->db->tableexists('ticket')){
        $this->forge->droptable('ticket');}
if ($this->db->tableexists('price')){
        $this->forge->droptable('price');}
if ($this->db->tableexists('session')){
        $this->forge->droptable('session');}
if ($this->db->tableexists('movie')){
        $this->forge->droptable('movie');}
if ($this->db->tableexists('seat')){
        $this->forge->droptable('seat');}
if ($this->db->tableexists('hall')){
        $this->forge->droptable('hall');}
        
	}
}


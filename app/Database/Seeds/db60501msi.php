<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class db60501msi extends Seeder
{
	public function run()
	{
       $data = [
                'Hall_id' => '1',
                'Hall_name' => '2D'
        ];
$this->db->table('hall')->insert($data);
        $data = [
                'Hall_id' => '2',
                'Hall_name' => '3D'
        ];
$this->db->table('hall')->insert($data);
        $data = [
                'Hall_id' => '3',
                'Hall_name' => '4D'
        ];
        $this->db->table('hall')->insert($data);

        $data = [
                'Movie_id' => '1',
                'Movie_name'=> 'Оно 2',
                'Length'=>'01:47:00'
        ];
$this->db->table('movie')->insert($data);
        $data = [
                'Movie_id' => '2',
                'Movie_name'=> 'Kingsman 2',
                'Length'=>'02:00:00'
        ];
$this->db->table('movie')->insert($data);
        $data = [
                'Movie_id' => '3',
                'Movie_name'=> 'Я легенда',
                'Length'=>'01:55:00'
        ];
$this->db->table('movie')->insert($data);
        $data = [
                'Movie_id' => '4',
                'Movie_name'=> 'Движение вверх',
                'Length'=>'02:13:00'
        ];
$this->db->table('movie')->insert($data);
        $data = [
                'Movie_id' => '5',
                'Movie_name'=> 'Джуманджи 2',
                'Length'=>'01:59:00'
        ];
        $this->db->table('movie')->insert($data);

        

        $data = [
            
            'Seat_id'=> '1',
            'Hall_id'=>'1',
            'Row_id' => '1',
            'Number_id' => '1',
            'Price_catg' => '1'
        ];
$this->db->table('seat')->insert($data);
$data = [
            
            'Seat_id'=> '2',
            'Hall_id'=>'1',
            'Row_id' => '1',
            'Number_id' => '2',
            'Price_catg' => '1'
        ];
$this->db->table('seat')->insert($data);
$data = [
            
            'Seat_id'=> '3',
            'Hall_id'=>'2',
            'Row_id' => '2',
            'Number_id' => '4',
            'Price_catg' => '2'
        ];
$this->db->table('seat')->insert($data);
$data = [
            
            'Seat_id'=> '4',
            'Hall_id'=>'2',
            'Row_id' => '2',
            'Number_id' => '4',
            'Price_catg' => '2'
        ];
$this->db->table('seat')->insert($data);
$data = [
            
            'Seat_id'=> '5',
            'Hall_id'=>'3',
            'Row_id' => '5',
            'Number_id' => '3',
            'Price_catg' => '3'
        ];
$this->db->table('seat')->insert($data);
$data = [
            
            'Seat_id'=> '6',
            'Hall_id'=>'3',
            'Row_id' => '2',
            'Number_id' => '4',
            'Price_catg' => '3'
        ];
$this->db->table('seat')->insert($data);
$data = [
            
            'Seat_id'=> '7',
            'Hall_id'=>'3',
            'Row_id' => '2',
            'Number_id' => '4',
            'Price_catg' => '3'
        ];
$this->db->table('seat')->insert($data);
$data = [
            
            'Seat_id'=> '8',
            'Hall_id'=>'1',
            'Row_id' => '2',
            'Number_id' => '4',
            'Price_catg' => '1'
        ];
$this->db->table('seat')->insert($data);
        $data = [
            
            'Seat_id'=> '9',
            'Hall_id'=>'2',
            'Row_id' => '2',
            'Number_id' => '4',
            'Price_catg' => '2'
        ];
$this->db->table('seat')->insert($data);
        $data = [
            
            'Seat_id'=> '10',
            'Hall_id'=>'3',
            'Row_id' => '2',
            'Number_id' => '4',
            'Price_catg' => '3'
        ];
        $this->db->table('seat')->insert($data);

        $data = [
            
            'id'=> '1',
            'Hall_id' => '1',
            'Movie_id' => '1',
            'StartDate' => '2017-11-23 18:00:00',
            'Price' => '250'
        ];
$this->db->table('session')->insert($data);
        $data = [
            
            'id'=> '2',
            'Hall_id' => '2',
            'Movie_id' => '3',
            'StartDate' => '2017-10-15 18:30:00',
            'Price' => '250'
        ];
$this->db->table('session')->insert($data);
        $data = [
            
            'id'=> '3',
            'Hall_id' => '3',
            'Movie_id' => '2',
            'StartDate' => '2017-11-08 19:00:00',
            'Price' => '250'
        ];
$this->db->table('session')->insert($data);
        $data = [
            
            'id'=> '4',
            'Hall_id' => '2',
            'Movie_id' => '5',
            'StartDate' => '2018-01-10 16:00:00',
            'Price' => '250'
        ];
$this->db->table('session')->insert($data);
        $data = [
            
            'id'=> '5',
            'Hall_id' => '3',
            'Movie_id' => '4',
            'StartDate' => '2017-10-05 13:20:00',
            'Price' => '250'
        ];
$this->db->table('session')->insert($data);
        $data = [
            
            'id'=> '6',
            'Hall_id' => '1',
            'Movie_id' => '2',
            'StartDate' => '2017-10-05 21:00:00',
            'Price' => '250'
        ];
$this->db->table('session')->insert($data);
        $data = [
            
            'id'=> '7',
            'Hall_id' => '2',
            'Movie_id' => '1',
            'StartDate' => '2017-09-10 18:10:00',
            'Price' => '250'
        ];
        $this->db->table('session')->insert($data);

        $data = [
            
            'Ticket_id'=> '1',
            'Session_id' => '1',
            'Seat_id' => '1',
            'FullName' => 'Котельников Вячеслав Сергеевич'
        ];
$this->db->table('ticket')->insert($data);
$data = [
            
            'Ticket_id'=> '2',
            'Session_id' => '1',
            'Seat_id' => '1',
            'FullName' => 'Мазия Татьяна Николаевна'
        ];
$this->db->table('ticket')->insert($data);
$data = [
            
            'Ticket_id'=> '3',
            'Session_id' => '5',
            'Seat_id' => '3',
            'FullName' => 'Мазия Игорь Станиславович'
        ];
$this->db->table('ticket')->insert($data);
$data = [
            
            'Ticket_id'=> '4',
            'Session_id' => '2',
            'Seat_id' => '4',
            'FullName' => 'Кузин Дмитрий Алесандрович'
        ];
$this->db->table('ticket')->insert($data);
$data = [
            
            'Ticket_id'=> '5',
            'Session_id' => '4',
            'Seat_id' => '5',
            'FullName' => 'Гармс Яков Фридрихович'
        ];
$this->db->table('ticket')->insert($data);
$data = [
            
            'Ticket_id'=> '6',
            'Session_id' => '7',
            'Seat_id' => '6',
            'FullName' => 'Гармс Любовь Яковлевна'
        ];
$this->db->table('ticket')->insert($data);
$data = [
            
            'Ticket_id'=> '7',
            'Session_id' => '1',
            'Seat_id' => '7',
            'FullName' => 'Лянтин Илья Сергеевич'
        ];
$this->db->table('ticket')->insert($data);
$data = [
            
            'Ticket_id'=> '8',
            'Session_id' => '6',
            'Seat_id' => '8',
            'FullName' => 'Смагин Глеб Сергеевич'
        ];
$this->db->table('ticket')->insert($data);
$data = [
            
            'Ticket_id'=> '9',
            'Session_id' => '2',
            'Seat_id' => '9',
            'FullName' => 'Мищенко Ярослав Игоревич'
        ];
$this->db->table('ticket')->insert($data);
$data = [
            
            'Ticket_id'=> '10',
            'Session_id' => '4',
            'Seat_id' => '10',
            'FullName' => 'Мазия Станислав Игоревич'
        ];
        $this->db->table('ticket')->insert($data);
	}
}


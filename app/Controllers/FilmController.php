<?php namespace App\Controllers;

use App\Models\FilmModel;
use App\Models\session;
use App\Models\price;
use App\Models\GetAll;
use Aws\S3\S3Client;
//use CodeIgniter\Controller;

class FilmController extends BaseController
{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            //return redirect()->to('/auth/login');
        }

        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        }
        else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;

        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search')))
        {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        }
        else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;

        helper(['form','url']);

        $model = new FilmModel();
        $data ['region'] = $model->getFilm(null, $search)->paginate($per_page, 'group1');
        $data['pager'] = $model->pager;
        
        echo view('Film/FilmViewAll', $this->withIon($data));

    }

    public function view($id = null) //отображение  записei seansa
    {
        if (!$this->ionAuth->loggedIn())
        {
            //return redirect()->to('/auth/login');
        }
        $model = new FilmModel();
        
        $data ['polling_station'] = $model->getFilm($id);
        echo view('Film/FilmView', $this->withIon($data));

    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        
        $model = new GetAll();
        $data ['H'] = $model->get(3);
        $data ['F'] = $model->get(1);
        //$data ['auto'] = $model->get();

        echo view('Film/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);
        if ($this->request->getMethod() === 'post' && $this->validate([
                'Hall_id' => 'required',
                'Movie_id'  => 'required',
                'StartDate'  => 'required', 
                'Price' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]',                      
            ]))
        {
            $insert=null;
            $file = $this->request->getFile('picture');
        if ($file->getSize() != 0) {
            //подключение хранилища
            $s3 = new S3Client([
                'version' => 'latest',
                'region' => 'us-east-1',
                'endpoint' => getenv('S3_ENDPOINT'),
                'use_path_style_endpoint' => true,
                'credentials' => [
                'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                ],
            ]);
            //получение расширения имени загруженного файла
            $ext = explode('.', $file->getName());
            $ext = $ext[count($ext) - 1];
            //загрузка файла в хранилище
            $insert = $s3->putObject([
                'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                //генерация случайного имени файла
                'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                'Body' => fopen($file->getRealPath(), 'r+')
            ]);

        }

            $model = new session();

            //тут моросит илья
            $data=([
                'Hall_id' => $this->request->getPost('Hall_id'),
                'Movie_id'  => $this->request->getPost('Movie_id'),
                'StartDate'  => $this->request->getPost('StartDate'), 
                'Price' => $this->request->getPost('Price'),
            ]);
            if(!is_null($insert))
            {
                $data['picture_url'] = $insert['ObjectURL'];
            }
                $model->save($data);
                session()->setFlashdata('message', lang('Session_create_success'));
                return redirect()->to('/FilmController');
        }
        else
        {
            return redirect()->to('/Film/create')->withInput();
        }



           
            // $model->save([
        //         'Hall_id' => $this->request->getPost('Hall_id'),
        //         'Movie_id' => $this->request->getPost('Movie_id'),
        //         'StartDate' => $this->request->getPost('StartDate'),
        //         'Price' => $this->request->getPost('Price'),
        //         // 'picture_url' => $this->request->getPost('picture_url')
        //     ]);
        //     //session()->setFlashdata('message', lang('Curating.Session_create_success'));
                                  
        //     if (!is_null($insert))
        //     $data['picture_url'] = $insert['ObjectURL'];

        //     // session()->setFlashdata('message', lang('Curating.Session_create_success'));
            
        //     return redirect()->to('/FilmController');
        // }
        // else
        // {
        //     return redirect()->to('/Film/create')->withInput();
        // }
    }


    public function edit($id){
        if (!$this->ionAuth->loggedIn()){
            return redirect()->to('/auth/login');
        }
        $model = new session();
        helper(['form']);
        $data ['polling_station'] = $model->getSes($id);
        $model = new GetAll();
        $data ['H'] = $model->get(3);
        //$data ['P'] = $model->get(2);


        $data ['validation'] = \Config\Services::validation();
        echo view('Film/edit', $this->withIon($data));
    }
    public function update()
    {
        helper(['form','url']);
        echo '/FilmController/edit/'.$this->request->getPost('Session_id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'StartDate' => 'required',
                'Hall_id'  => 'required',
        
                'Movie_id' => 'required',
                'Price' => 'required'
            ]))
        {

            $model = new session();
            $model->save([
                'id' => $this->request->getPost('id'),
                'Hall_id' => $this->request->getPost('Hall_id'),
                'Movie_id' => $this->request->getPost('Movie_id'),
                'StartDate' => $this->request->getPost('StartDate'),
                'Price' => $this->request->getPost('Price')
            ]);

            
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/FilmController');
        }
        else
        {
           return redirect()->to('/FilmController/edit/'.$this->request->getPost('Session_id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new session();
        $model->where('id',$id)->delete();  
        return redirect()->to('/FilmController');
    }
}

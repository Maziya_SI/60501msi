<?php namespace App\Models;
use CodeIgniter\Model;
class price extends Model
{
    protected $table= 'price'; //таблица, связанная с моделью
    protected $allowedFields = ['id', 'Session_id', 'Price_catg', 'Price'];
    

    public function getSes()
    {
        return $this->select('s.id')->from('session s')->distinct()->orderby('id', 'desc')->limit(1)->first();
    }
    public function getPrice($i)
    {
        return $this->select('Price')->distinct()->where('Price_catg', $i)->first();
    }    

}

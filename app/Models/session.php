<?php namespace App\Models;
use CodeIgniter\Model;
class session extends Model
{
    protected $table= 'session'; //таблица, связанная с моделью
    protected $allowedFields = ['id', 'Hall_id', 'Movie_id', 'StartDate', 'Price', 'picture_url'];
        
    public function getSes($id)
    {
        return $this->select('s.StartDate, s.id, h.Hall_name, h.Hall_id, s.Price,  m.Movie_name, m.Movie_id, s.picture_url')->distinct()->from('session s')->join('hall h', 's.Hall_id=h.Hall_id')->join('movie m', 'm.Movie_id=s.Movie_id')->where('s.id', $id)->first();
    }
}

<?php namespace App\Models;
use CodeIgniter\Model;
class GetAll extends Model
{
    protected $table= 'movie'; //таблица, связанная с моделью
    protected $allowedFields = ['Movie_id', 'Movie_name', 'picture_url'];
    public function get($id = null)
    {
        if ($id==1) {
            return $this->findAll();
        }
        else if ($id==3){
            return $this->select('h.Hall_id, h.Hall_name')->distinct()->from('hall h')->findAll();
        }
        else {
            return $this->select(nextval('session_id_seq'));
        }
    }
}

//SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='session' 

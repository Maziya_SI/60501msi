<?php namespace App\Models;
use CodeIgniter\Model;
class FilmModel extends Model
{
    protected $table= 'movie'; //таблица, связанная с моделью
    protected $allowedFields = ['Movie_id', 'Movie_name', 'Length', 'picture_url'];
    public function getFilm($id = null, $search = '')
    {
        
        if (!is_null($id)) {
            return $this->select('s.StartDate, s.id, h.Hall_name, h.Hall_id, s.Price, m.Movie_name, m.Movie_id, m.Length, m.picture_url')->distinct()->from('session s')->join('hall h', 's.Hall_id=h.Hall_id')->join('movie m', 'm.Movie_id=s.Movie_id')->where('s.Movie_id', $id)->findAll();
        }
        else {                       
            return $this->select('*')->like('movie.Movie_name', $search,'both', null, false);
        }
    }
}


/*
SELECT s.StartDate, h.Hall_name, p.Price, m.Movie_name FROM session AS s 
JOIN hall AS h ON s.Hall_id=h.Hall_id 
JOIN price AS p ON s.Session_id=p.Session_id
JOIN movie AS m ON m.Movie_id=s.Movie_id
WHERE  s.Movie_id=1
*/
